Source: emacs-theme-gruvbox
Section: lisp
Priority: optional
Maintainer: Debian Emacsen team <debian-emacsen@lists.debian.org>
Uploaders: Nicholas D Steeves <sten@debian.org>
Build-Depends: debhelper-compat (= 13)
             , dh-elpa
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/emacsen-team/emacs-theme-gruvbox
Vcs-Git: https://salsa.debian.org/emacsen-team/emacs-theme-gruvbox.git
Homepage: https://github.com/greduan/emacs-theme-gruvbox

Package: elpa-gruvbox-theme
Architecture: all
Depends: ${elpa:Depends}, ${misc:Depends}
Recommends: emacs
Enhances: emacs
Description: retro-groove colour scheme for Emacs
 Gruvbox is a popular retro groove color scheme for Emacs.  It is a port of
 Pavel Pertsev's original Vim theme.  This theme uses far less blue than most,
 and eschews candy colours for a warmer colour scheme that retains contrast.
 It seems to be retro insomuch as it makes use of various green, beige, peach,
 and orange tones.  There is nothing that matches an astonishingly bright RGB
 LED here.
 .
 Gruvbox contains light and dark themes in hard, medium, and soft variants,
 and it uses Autothemer's Theme Variance Architecture to do this.
